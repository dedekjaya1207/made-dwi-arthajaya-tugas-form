<!DOCTYPE html>
<html>
<head>
    <title>Laporan</title>
    <style>
    body{
        background-color: black;
    }
    .hasil{
        color: white;
        text-align: center;
        font-family: "Times New Roman", Times, serif;
        padding: 10% 30%;
    }
    </style>
</head>
<body>
<div class= hasil>
    <h1> Halaman Hasil </h1>
    <?php 
        $nama = $_POST["nama"];
        $pelajaran = $_POST["pelajaran"];
        $uts = $_POST["uts"];
        $uas = $_POST["uas"];
        $tugas = $_POST["tugas"];

        $nilai = ($tugas*0.15)+($uas*0.5)+($uts*0.35);
        if($nilai >=90){
            $grade = "A";
        }elseif($nilai >=70){
            $grade = "B";
        }elseif($nilai >=50){
            $grade = "C";
        }else{
            $grade ="D";
        }
        echo "Kepada siswa/i atas nama $nama, dalam mata pelajaran $pelajaran
         anda mendapat nilai UTS $uts, nilai UAS $uas, dengan nilai rata-rata $nilai, sehingga dalam mata pelajaran ini 
         anda mendapat predikat $grade.";
    ?>  
</div>
</body>
</html>